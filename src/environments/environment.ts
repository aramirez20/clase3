// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBXndH90VFV9guP1qvK9AH4mSOec63-NJk",
    authDomain: "aarm-ng-21.firebaseapp.com",
    projectId: "aarm-ng-21",
    storageBucket: "aarm-ng-21.appspot.com",
    messagingSenderId: "422971837900",
    appId: "1:422971837900:web:4deba8f8f0c1954c46e169",
    measurementId: "G-EQ8ESFYX3D"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
