import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import firebase from 'firebase';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  title: string = "Clase 3";
  loginForm: FormGroup;
  showUser: boolean = false;
  emailStatus: string;

  
  constructor(
    private formBuilder: FormBuilder,
    private _authService: AuthService,
    private _router: Router
  ) { 
      this.loginForm = this.formBuilder.group({
        email: [null, [Validators.required, Validators.email, Validators.minLength(5)]],
        password: [null, [Validators.required, Validators.minLength(6)]]
      });
  }

  ngOnInit(): void {
    this._authService.checkCurrentUser().then( res => {
         console.log({res});
    }).catch(err => {
      console.error(err)
    });
  }
 
  public async onSubmit(): Promise<any>{
    const {email, password} = this.loginForm.controls;
    const validate: boolean = this.validateFields(email, password);
    if(validate){
      // TODO login Service
      this._authService.singIn(email.value, password.value).then((res: firebase.auth.UserCredential) => {
        console.log(JSON.stringify(res));
        this.showUser = true;
        this.emailStatus = res.user.emailVerified ? 'Verificado' : 'No verificado';
        this._router.navigate(['/inicio']); 
      })
    }
  }

  public validateFields(email: AbstractControl, password: AbstractControl): boolean {
    if(this.loginForm.valid){
      return true;

    }  
    if(!email.valid && !password.valid){
          alert('Los campos son obligatorios');
          return false;
      }
      if(!email.valid){
        alert('El email es incorrecto');
        return false;
      }
      if(!password.valid){
        alert('Debes poner una contraseña');
        return false;
        
      }
        return true;
      }
  }


 