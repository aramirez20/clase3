import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule} from '@angular/forms'
import { environment } from 'src/environments/environment';


// COMPONENTS
import { LoginComponent } from './domain/views/login/login.component';
import { RegisterComponent } from './domain/views/register/register.component';
import { NotFoundPage } from './domain/views/notFound/notFound.component';

// ANGULAR MATERIAL LIBRARIES
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';

// LIBRARIES
import {AngularFireModule} from '@angular/fire';
import {AngularFireAuthModule} from '@angular/fire/auth';
import { HomeComponent } from './domain/views/home/home.component';
import { DishcardComponent } from './domain/components/dishcard/dishcard.component';
import { NavbarComponent } from './domain/components/navbar/navbar.component';
import { DishComponent } from './domain/views/dish/dish.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    NotFoundPage,
    HomeComponent,
    DishcardComponent,
    NavbarComponent,
    DishComponent,
    //MaxlengthPipe
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    // MATERIAL
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatToolbarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
